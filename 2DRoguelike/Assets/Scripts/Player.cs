﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MovingObject {

	public int enemyDamage = 1;
	public int wallDamage = 1;
	public int pointsPerFood = 10;
	public int pointsPerSoda = 20;
	public float restartLevelDelay = 1f;
	public Text foodText;

	public AudioClip moveSound1;
	public AudioClip moveSound2;
	public AudioClip eatSound1;
	public AudioClip eatSound2;
	public AudioClip drinkSound1;
	public AudioClip drinkSound2;
	public AudioClip gameOverSound;

	private Animator animator;
	public int food;

	public float numberEnemy;

	public float levelStartDelay = 2f;

	// Use this for initialization
	protected override void Start () 
	{
		animator = GetComponent<Animator> ();

		food = GameManager.instance.playerFoodPoints;

		foodText.text = "Fullness: " + food;

		base.Start ();

	}

	private void OnDisable()
	{
		GameManager.instance.playerFoodPoints = food;
	}

	// Update is called once per frame
	void Update () 
	{
		if (!GameManager.instance.playersTurn)
			return;

		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		numberEnemy = enemies.Length;

		int horizontal = 0;
		int vertical = 0;

		horizontal = (int) Input.GetAxisRaw("Horizontal");

		vertical = (int) Input.GetAxisRaw ("Vertical");	

		if (horizontal != 0)
			vertical = 0;

		if (horizontal != 0 || vertical != 0)
			AttemptMove<Wall> (horizontal, vertical);

	}

	protected override void AttemptMove <T> (int xDir, int yDir)
	{
		food--;
		foodText.text = "Fullness: " + food;
		base.AttemptMove <T> (xDir, yDir);

		RaycastHit2D hit;
		if (Move (xDir, yDir, out hit)) 
		{
			SoundManager.instance.RandomizeSfx (moveSound1, moveSound2);
		}


		CheckIfGameOver ();

		GameManager.instance.playersTurn = false;
	}

	private void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Exit" && numberEnemy == 0) {
			Invoke ("Restart", restartLevelDelay);
			enabled = false;
		} else if (other.tag == "Food") {
			food += pointsPerFood;

			foodText.text = "+" + pointsPerFood + " Fullness: " + food;

			SoundManager.instance.RandomizeSfx (eatSound1, eatSound2);

			other.gameObject.SetActive (false);
		} else if (other.tag == "Soda") {
			food += pointsPerFood;

			foodText.text = "+" + pointsPerSoda + " Fullness: " + food;

			SoundManager.instance.RandomizeSfx (drinkSound1, drinkSound2);

			other.gameObject.SetActive (false);
		} else if (other.tag == "Crypt" && numberEnemy == 0) 
		{
			print ("crypt touch");
			GameManager.instance.GameWin ();
			enabled = false; 
		}

	}


	protected override void OnCantMove <T> (T component)
	{	
		print ("collide");

		//if (gameObject.tag == "Wall" )
		//{
			Wall hitWall = component as Wall;
			hitWall.DamageWall (wallDamage);
			animator.SetTrigger ("playerChop");
			print ("chop"); 
		//}
		//else 
		//{
		//Enemy hitEnemy = component as Enemy;
		//hitEnemy.DamageEnemy (enemyDamage); 
		//animator.SetTrigger ("playerChop");
			//print ("hit");
		//}
	}

	private void Restart()
	{
		SceneManager.LoadScene (0); 

	}


	public void LoseFood (int loss) 
	{
		animator.SetTrigger ("playerHit");
		food -= loss;
		foodText.text = "-" + loss + " Fullness: " + food;

		CheckIfGameOver ();
	}

	private void CheckIfGameOver()
	{
		if (food <= 0) 
		{
			SoundManager.instance.PlaySingle(gameOverSound);
			SoundManager.instance.musicSource.Stop ();
			GameManager.instance.GameOver ();
		}
	}

}
