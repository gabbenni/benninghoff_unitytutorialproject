﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour {

	public float levelStartDelay = 2f;

	public float turnDelay = .1f;

	public static GameManager instance = null;

	public BoardManager boardScript;

	public int playerFoodPoints = 100;

	[HideInInspector] public bool playersTurn = true;

	public float timer = 80f; 

	private int level = 1;

	private List<Enemy> enemies;

	private bool enemiesMoving;

	private Text levelText;

	private GameObject levelImage; 

	private bool doingSetup;



	// Use this for initialization
	void Awake () 
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
		DontDestroyOnLoad (gameObject);

		enemies = new List<Enemy> ();

		levelImage = GameObject.Find ("LevelImage");
		levelText = GameObject.Find ("LevelText").GetComponent<Text> ();

		boardScript = GetComponent<BoardManager> ();
		InitGame ();


	}

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
	static public void CallbackInitialization()
	{
		//register the callback to be called everytime the scene is loaded
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	//This is called each time a scene is loaded.
	static private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
	{
		instance.level++;
		instance.InitGame();
	}


	void InitGame ()
	{
		doingSetup = true;
		levelImage = GameObject.Find ("LevelImage");
		levelText = GameObject.Find ("LevelText").GetComponent<Text> ();

		levelImage.GetComponent<Image>().enabled = (true);
		levelText.GetComponent<Text>().enabled =  (true);
		levelText.text = "Hour " + level;

		Invoke ("HideLevelImage", levelStartDelay);

		enemies.Clear();
		boardScript.SetupScene (level);


	}

	private void HideLevelImage()
	{
		levelImage.GetComponent<Image>().enabled = (false);
		levelText.GetComponent<Text>().enabled =  (false);
		doingSetup = false;
	}


	public void GameOver ()
	{
		levelText.text = "After " + level + " hours, you starved.";
		levelImage.GetComponent<Image>().enabled =  (true);
		levelText.GetComponent<Text>().enabled =  (true);
		enabled = false;
	}

	public void GameWin ()
	{
		print ("Win!");

		levelText.text = "You survived the night to hunt again!";
		levelImage.GetComponent<Image>().enabled =  (true);
		levelText.GetComponent<Text>().enabled =  (true);
		enabled = false;
	}



	// Update is called once per frame
	void Update () 
	{
		timer -= Time.deltaTime;


		if (timer <= 0) 
		{
			GameOver();
		}

		if (playersTurn || enemiesMoving || doingSetup)
			return;

		StartCoroutine(MoveEnemies ());

	}

	public void AddEnemyToList(Enemy script)
	{
		enemies.Add (script);
	}


	IEnumerator MoveEnemies()
	{
		enemiesMoving = true;
		yield return new WaitForSeconds(turnDelay);
		if (enemies.Count == 0) 
		{
			yield return new WaitForSeconds (turnDelay);
		}

		for (int i = 0; i < enemies.Count; i++)
		{	enemies [i].MoveEnemy ();
			yield return new WaitForSeconds (enemies [i].moveTime);
		}

		playersTurn = true;
		enemiesMoving = false;
	}

}
