﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialScript : MonoBehaviour {



	public Sprite face1; 
	public Sprite face2;
	public Sprite face3;
	public Sprite face4;
	public Sprite face5;

	private float timer; 
	private Image image;

	// Use this for initialization
	void Start () {

		timer = GameManager.instance.timer;
		image = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {

		timer = GameManager.instance.timer;

		if (timer <= 67 && timer >= 54) 
		{
			image.sprite = face1;
		}

		if (timer <= 53 && timer >= 40) 
		{
			image.sprite = face2;
		}

		if (timer <= 39 && timer >= 26) 
		{
			image.sprite = face3;
		}

		if (timer <= 25 && timer >= 13)
		{
			image.sprite = face4;
		}

		if (timer <= 12 && timer >= 1) 
		{
			image.sprite = face5;
		}
	}
}
