﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MovingObject {

	public int playerDamage;

	private Animator animator;
	private Transform target;
	private bool skipMove;
	public int hp = 3;
	public int loss = 1;
	public AudioClip enemyAttack1;
	public AudioClip enemyAttack2;
	public int pointsPerKill = 30;

	public Player playerScript;

	private bool killed = false;

	protected override void Start () 
	{
		GameManager.instance.AddEnemyToList (this);
		animator = GetComponent<Animator> ();
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		base.Start ();

		playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>(); 
	}
	


	protected override void AttemptMove <T> (int xDir, int yDir)
	{


		base.AttemptMove <T> (xDir, yDir);


	}

	public void MoveEnemy()
	{
		int xDir = 0;
		int yDir = 0;

		if (Mathf.Abs (target.position.x - transform.position.x) < float.Epsilon)
			yDir = target.position.y > transform.position.y ? -1 : 1;
		else
			xDir = target.position.x > transform.position.x ? -1 : 1;

		AttemptMove <Player> (xDir, yDir);
	}

	protected override void OnCantMove <T> (T component)
	{
		Player hitPlayer = component as Player;
		animator.SetTrigger ("enemyAttack");
		SoundManager.instance.RandomizeSfx (enemyAttack1, enemyAttack2);
		hp -= loss;
		if (hp <= 0 && killed == false) 
		{
			killed = true;
		}	
			
	}

	//public void DamageEnemy (int loss)
	//{

		//animator.SetTrigger ("enemyHit");

		//SoundManager.instance.RandomizeSfx (enemyAttack1, enemyAttack2);
		//print ("ouch");
		//hp -= loss;
		//if (hp <= 0) 
		//{
			//killed = true;
		//}	
	//}



	void Update ()
	{
		if (killed == true)
		{

			playerScript.food += pointsPerKill;
			playerScript.foodText.text = "+" + pointsPerKill + " Fullness: " + playerScript.food;
			animator.SetTrigger ("enemyAttack");
			SoundManager.instance.RandomizeSfx (enemyAttack1, enemyAttack2);
			gameObject.SetActive (false);
 		}
				
	} 
}